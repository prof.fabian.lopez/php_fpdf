<?php
require('./fpdf/fpdf.php');
class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    /// Image(imagen,x,y,tamaño)
    $this->Image('logo.jpeg',10,8,25);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
/// Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]] 
/// Cell(cuadro-->> 0,10,texto,borde,posicion,alineacion,relleno)    
// 0 -> sin borde
// por defecto muchas opciones vienen en false    
//$this->Cell(50,10,'Titulo de ejemplo',1,1,'C',true);
    $this->Cell(60,10,'Listado de personas',1,1,'C');
    // Salto de línea
    $this->Ln(20);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$archivo=fopen('padron.dat','r+') or die ("Error de apertura de archivo, consulte con el administrador...");

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->SetFont('Times','',12);

while(!feof($archivo))
{
	$linea=fgets($archivo);
	$datos=explode("|",$linea);
	$dni=$datos[2];
	$apellido=$datos[4];
	$nombres=$datos[5];
	
	$pdf->Cell(30,12,$dni,1,0,'C');
	$pdf->Cell(70,12,utf8_decode($apellido),1,0,'C');
	$pdf->Cell(70,12,utf8_decode($nombres),1,1,'C');
//	$pdf->ln(10);

}
// si es igual usuario e identica pass y ademas esta activa podría ingresar
$pdf->Output('','Padron.pdf');
fclose($archivo);
?>
